﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Data.SqlClient;

namespace SqlServerConsole
{
    [SuppressMessage("ReSharper", "CommentTypo")]
    class Program
    {
        static void Main(string[] args)
        {
            const string connectionString = "Data Source=localhost\\SQLEXPRESS;User Id=sa;Password=Pa$$w0rd;";
            const string databaseName = "EjemploSqlClient";
            const string tableOneName = "Alumno";
            const string tableTwoName = "Clase";
            const string tableJoinName = "Matricula";

            Console.WriteLine("Ejemplo de acceso a SQL Server con SqlClient provider");

            SqlConnection sqlConnection = null!;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                Console.WriteLine($"Conectado a SQL Server {sqlConnection.ServerVersion}");

                var sqlDropDatabase = $"DROP DATABASE IF EXISTS {databaseName};";
                var sqlDropDatabaseCommand = new SqlCommand(sqlDropDatabase, sqlConnection);
                sqlDropDatabaseCommand.ExecuteNonQuery();
                var sqlCreateDatabase = $"CREATE DATABASE {databaseName};";
                var sqlCreateDatabaseCommand = new SqlCommand(sqlCreateDatabase, sqlConnection);
                sqlCreateDatabaseCommand.ExecuteNonQuery();
                Console.WriteLine($"Base de datos {databaseName} re-creada");

                var sqlUseDatabase = $"USE {databaseName};";
                var sqlUseDatabaseCommand = new SqlCommand(sqlUseDatabase, sqlConnection);
                sqlUseDatabaseCommand.ExecuteNonQuery();
                Console.WriteLine($"Usando contexto de base de datos {databaseName}");

                var sqlCreateTableOne =
                    $"CREATE TABLE {tableOneName} ("
                    + "Id INT NOT NULL,"
                    + "Nombre VARCHAR(45) NOT NULL,"
                    + "Apellido VARCHAR(45) NOT NULL,"
                    + "PRIMARY KEY(Id))";
                var sqlCreateTableOneCommand = new SqlCommand(sqlCreateTableOne, sqlConnection);
                sqlCreateTableOneCommand.ExecuteNonQuery();
                Console.WriteLine($"Tabla {tableOneName} creada");

                var sqlCreateTableTwo =
                    $"CREATE TABLE {tableTwoName} ("
                    + "Id INT NOT NULL,"
                    + "Nombre VARCHAR(45) NOT NULL,"
                    + "Descripcion VARCHAR(MAX) NOT NULL,"
                    + "PRIMARY KEY(Id))";

                var sqlCreateTableTwoCommand = new SqlCommand(sqlCreateTableTwo, sqlConnection);
                sqlCreateTableTwoCommand.ExecuteNonQuery();
                Console.WriteLine($"Tabla {tableTwoName} creada");

                var sqlCreateTableJoin =
                    $"CREATE TABLE {tableJoinName} ("
                    + "Id INT NOT NULL,"
                    + "IdAlumno INT NOT NULL,"
                    + "IdClase INT NOT NULL,"
                    + "Fecha DATE NOT NULL,"
                    + "PRIMARY KEY(Id),"
                    + "FOREIGN KEY (IdAlumno) REFERENCES Alumno(Id),"
                    + "FOREIGN KEY (IdClase) REFERENCES Clase(Id))";
                var sqlCreateTableJoinCommand = new SqlCommand(sqlCreateTableJoin, sqlConnection);
                sqlCreateTableJoinCommand.ExecuteNonQuery();
                Console.WriteLine($"Tabla {tableJoinName} creada");

                // Ejercicio 1
                // Insertar alumnos Ted Codd, Martin Fowler, Greg Young y Udi Dahan

                // Ejercicio 2
                // Insertar clases Sistemas Distribuidos, Bases de Datos, CQRS y Technical English

                // Ejercicio 3
                // Matricular a Ted Codd en Bases de datos en cualquier fecha de 1971
                // Matricular a Martin Fowler en Sistemas Distribuidos y Bases de datos en cualquier fecha de 2000
                // Matricular a Greg Young en Sistemas Distribuidos, Bases de datos y CQRS en cualquier fecha de 2003
                // Matricular a Udi Dahan en Sistemas Distribuidos, Bases de datos y CQRS en cualquier fecha de 2010

                // Ejercicio 4
                // Mostrar en consola todos los alumnos
                // Mostrar en consola todas las clases

                // Ejercicio 5
                // Mostrar en consola todas las clases y el número de alumnos que se han matriculado

                // Ejercicio 6
                // Mostrar en consola todas las clases y el número de alumnos que se han matriculado antes de 2000

                // Ejercicio 7
                // Mostrar en consola todos los alumnos y el número de clases en el que se han matriculado


                Console.WriteLine("Presiona cualquier tecla para finalizar..");
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            finally
            {
                CloseConnection(sqlConnection);
            }
        }

        private static void CloseConnection(SqlConnection sqlConnection)
        {
            try
            {
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
